package hr.fer.tel.infmre.bellmanford.enums;


/**
 * Collection of edge types that can
 * be directed or undirected.
 *
 * @author Matija Pevec
 */
public enum EdgeType {
    DIRECTED, UNDIRECTED;
}
