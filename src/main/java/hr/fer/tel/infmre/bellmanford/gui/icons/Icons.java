package hr.fer.tel.infmre.bellmanford.gui.icons;

import hr.fer.tel.infmre.bellmanford.utilities.Utilites;

import javax.swing.*;

/**
 * Collection of statically pre-loaded
 * icons used in this application.
 */
public class Icons {

    /**
     * Next step image icon
     */
    public static ImageIcon nextStep            = Utilites.loadImageFromJavaPack("media/StepForward24.gif");

    /**
     * Previous step image icon
     */
    public static ImageIcon previousStep        = Utilites.loadImageFromJavaPack("media/StepBack24.gif");

    /**
     * Next iteration image icon
     */
    public static ImageIcon nextIteration       = Utilites.loadImageFromJavaPack("media/FastForward24.gif");

    /**
     * Previous iteration image icon
     */
    public static ImageIcon previousIteration   = Utilites.loadImageFromJavaPack("media/Rewind24.gif");

    /**
     * Reset image icon
     */
    public static ImageIcon reset               = Utilites.loadImageFromJavaPack("general/Undo24.gif");

    /**
     * Final solution image icon
     */
    public static ImageIcon finalSolution       = Utilites.loadImageFromJavaPack("general/Export24.gif");

    /**
     * Rewind image icon
     */
    public static ImageIcon rewind              = Utilites.loadImageFromJavaPack("navigation/Back24.gif");
}
