package hr.fer.tel.infmre.bellmanford.gui.components;

import javax.swing.*;
import java.awt.*;

/**
 * Custom JPanel with rounded corners.
 */
public class RoundedPanel extends JPanel {

    /**
     * Corner radius.
     */
    private int arcDimension;

    /**
     * Initializes new RoundedPanel with given corner radius.
     *
     * @param arcDimension Corner radius
     */
    public RoundedPanel(int arcDimension) {
        super();
        this.arcDimension = arcDimension;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Dimension arcs = new Dimension(arcDimension, arcDimension);
        int width = getWidth();
        int height = getHeight();
        Graphics2D graphics = (Graphics2D) g;
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        //Draws the rounded opaque panel with borders.
        graphics.setColor(getBackground());
        graphics.fillRoundRect(0, 0, width - 1, height - 1, arcs.width, arcs.height); //paint background
        graphics.setColor(getForeground());
        graphics.drawRoundRect(0, 0, width - 1, height - 1, arcs.width, arcs.height); //paint border
    }
}
