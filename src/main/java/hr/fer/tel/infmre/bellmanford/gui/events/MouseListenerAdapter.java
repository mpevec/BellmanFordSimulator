package hr.fer.tel.infmre.bellmanford.gui.events;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Mouse listener adapter. Offers default empty implementations for {@link MouseListener}
 */
public interface MouseListenerAdapter extends MouseListener {

    @Override
    default void mouseClicked(MouseEvent mouseEvent) {
    }

    @Override
    default void mousePressed(MouseEvent mouseEvent) {
    }

    @Override
    default void mouseReleased(MouseEvent mouseEvent) {
    }

    @Override
    default void mouseEntered(MouseEvent mouseEvent) {
    }

    @Override
    default void mouseExited(MouseEvent mouseEvent) {
    }
}
