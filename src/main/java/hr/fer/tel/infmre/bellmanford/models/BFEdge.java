package hr.fer.tel.infmre.bellmanford.models;

import hr.fer.tel.infmre.bellmanford.enums.EdgeType;

import java.util.Objects;

/**
 * This class represents an edge in Bellman-Ford algorithm.
 *
 * @author Matija Pevec
 */
public class BFEdge {

    private BFNode start;
    private BFNode end;
    private Integer weight;
    private EdgeType edgeType;
    private String edgeId;

    public BFEdge(BFNode start, BFNode end, Integer weight, EdgeType edgeType) {
        this.start = start;
        this.end = end;
        this.weight = weight;
        this.edgeType = edgeType;
        this.edgeId = start.getId() + "-" + end.getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BFEdge bfEdge = (BFEdge) o;
        return Objects.equals(start, bfEdge.start) &&
                Objects.equals(end, bfEdge.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }

    public BFNode getStart() {
        return start;
    }

    @Override
    public String toString() {
        return "{" + start +
                ", " + end +
                ", " + weight +
                ", " + edgeType +
                '}';
    }

    public BFNode getEnd() {
        return end;
    }

    public Integer getWeight() {
        return weight;
    }

    public EdgeType getEdgeType() {
        return edgeType;
    }

    public String getEdgeId() {
        return edgeId;
    }
}
