package hr.fer.tel.infmre.bellmanford.utilities;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import javax.swing.ImageIcon;

public class Utilites {

	public static String loadFileFromResources(String fileName) {
		try {
			InputStream is = Utilites.class.getClassLoader().getResourceAsStream(fileName);
			return IOUtils.toString(is, Charset.defaultCharset());
		} catch (IOException e) {
			throw new RuntimeException("Could not load styles file.");
		}
	}
	
	public static ImageIcon loadImageFromJavaPack(String imageLocation) {
        URL imageURL = Utilites.class.getClassLoader().getResource("toolbarButtonGraphics/" + imageLocation);
        return new ImageIcon(imageURL);
	}
}
