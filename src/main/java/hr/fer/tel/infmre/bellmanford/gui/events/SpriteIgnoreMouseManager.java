package hr.fer.tel.infmre.bellmanford.gui.events;

import org.graphstream.ui.graphicGraph.GraphicElement;
import org.graphstream.ui.graphicGraph.GraphicNode;
import org.graphstream.ui.graphicGraph.GraphicSprite;
import org.graphstream.ui.view.util.DefaultMouseManager;

import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * Modifies {@link DefaultMouseManager} in a way that it ignores events occured
 * on sprites located within nodes and passe them on to nodes.
 */
public class SpriteIgnoreMouseManager extends DefaultMouseManager {

    @Override
    protected void elementMoving(GraphicElement element, MouseEvent event) {
        elementEventOccured(element, event, (el, ev) -> super.elementMoving(el, ev));
    }

    @Override
    protected void mouseButtonPressOnElement(GraphicElement element, MouseEvent event) {
        elementEventOccured(element, event, (el, ev) -> super.mouseButtonPressOnElement(el, ev));
    }

    @Override
    protected void mouseButtonReleaseOffElement(GraphicElement element, MouseEvent event) {
        elementEventOccured(element, event, (el, ev) -> super.mouseButtonReleaseOffElement(el, ev));
    }

    @Override
    protected void mouseButtonRelease(MouseEvent event, Iterable<GraphicElement> elementsInArea) {
        List<GraphicElement> elements = new LinkedList<>();
        elementsInArea.forEach(el -> {
            if (!(el instanceof GraphicSprite)) elements.add(el);
        });
        super.mouseButtonRelease(event, elements);
    }

    /**
     * Method handles event occuring on some {@link GraphicElement}
     *
     * @param element      Graphic element (sprite, node ,etc)
     * @param event        Event
     * @param eventHandler Event handler
     */
    private void elementEventOccured(GraphicElement element, MouseEvent event, BiConsumer<GraphicElement, MouseEvent> eventHandler) {
        if (element instanceof GraphicSprite) {
            GraphicSprite sprite = (GraphicSprite) element;
            GraphicNode node = sprite.getNodeAttachment();
            eventHandler.accept(node != null ? node : element, event);
        } else {
            eventHandler.accept(element, event);
        }
    }


}
