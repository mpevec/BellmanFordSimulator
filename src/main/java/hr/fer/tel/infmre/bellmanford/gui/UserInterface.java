package hr.fer.tel.infmre.bellmanford.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import hr.fer.tel.infmre.bellmanford.gui.actions.StatefulAbstractAction;
import hr.fer.tel.infmre.bellmanford.gui.components.RoundedPanel;
import hr.fer.tel.infmre.bellmanford.gui.events.MouseListenerAdapter;
import hr.fer.tel.infmre.bellmanford.gui.events.SpriteIgnoreMouseManager;
import hr.fer.tel.infmre.bellmanford.gui.icons.Icons;
import hr.fer.tel.infmre.bellmanford.models.BFDistance;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.ui.graphicGraph.GraphicElement;
import org.graphstream.ui.graphicGraph.GraphicSprite;
import org.graphstream.ui.graphicGraph.stylesheet.StyleConstants;
import org.graphstream.ui.layout.Layouts;
import org.graphstream.ui.spriteManager.Sprite;
import org.graphstream.ui.spriteManager.SpriteManager;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;

import hr.fer.tel.infmre.bellmanford.algorithm.IBellmanFord;
import hr.fer.tel.infmre.bellmanford.algorithm.impl.BellmanFordImpl;
import hr.fer.tel.infmre.bellmanford.models.AlgorithmStep;
import hr.fer.tel.infmre.bellmanford.models.BFGraph;
import hr.fer.tel.infmre.bellmanford.utilities.Utilites;

/**
 * Complete GUI for Bellman-Ford simulator.<br />
 * Uses Java JFC/Swing and GraphStream for displaying and
 * rendering data kept in {@link IBellmanFord} algorithm
 * implementation.
 */
public class UserInterface extends JFrame implements MouseListenerAdapter {

    /**
     * Data model with algorithm results.
     */
    private IBellmanFord model;

    /**
     * Total number of iterations.
     */
    private int numOfIterations;

    /**
     * Number of steps per each iteration.
     */
    private int numOfStepsInIteration;


    /**
     * Graph-stream object representing graph structure.
     */
    private Graph graph;

    /**
     * ID of user selected stargin node.
     */
    private Node startNode;


    /**
     * Sprite manager, used for old/new node distance values.
     */
    private SpriteManager smen;

    /**
     * Graph-stream's view object used for displaying graph.
     * Can be used as Component within Swing.
     */
    private View view;

    /**
     * Iteration/step number status label.
     */
    private JLabel statusLabel;

    /**
     * Panel containing status label.
     */
    private JPanel statusLabelPanel;


    /**
     * Default empty constructor.
     * Instantiates UserInterface by calling many helper
     * methods and building UI block by block.
     */
    public UserInterface() {
        initUI();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Basic UI intialization method.
     * Calls helper methods in order to build complete UI.
     */
    private void initUI() {
        basicUserInterfaceSetUp();
        setKeyboardEventsDispatcher();
        setUpActions();

        createGraphView();

        createMenuBar();
        createTopBar();
        createBottomBar();
    }

    /**
     * Method sets up some basic UI properties such as window
     * size, UI look and feel and so on.
     */
    private void basicUserInterfaceSetUp() {
        setTitle("Bellman-Ford Simulator v1.0");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(700, 400));
        setLayout(new BorderLayout());
        setButtonsState(false, false, false, false, false, false);
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignorable) {
            System.out.println(ignorable.getMessage());
        }
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
    }

    /**
     * Sets up interceptor catching all keyboard events globally.
     * Some events are processed immidately while others are passed
     * to target objects.
     * This actually blocks Graph-stream's catching of left,right
     * arrow key bindings, as they are used for navigation throughout
     * the algorithm steps. Graph-stream on the other hand uses very
     * same key bindings for moving graph on canvas. This method
     * will block dispatching left,right arrow key to Graph-stream.
     */
    private void setKeyboardEventsDispatcher() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager()
                .addKeyEventDispatcher(e -> {
                    boolean keyReleased = e.getID() != KeyEvent.KEY_RELEASED;
                    switch (e.getKeyCode()) {
                        case KeyEvent.VK_LEFT:
                            if (keyReleased && previousStepAction.isEnabled()) previousStepAction.actionPerformed(null);
                            break;
                        case KeyEvent.VK_RIGHT:
                            if (keyReleased && nextStepAction.isEnabled()) nextStepAction.actionPerformed(null);
                            break;
                        case KeyEvent.VK_R:
                            if (keyReleased && resetAction.isEnabled()) resetAction.actionPerformed(null);
                            break;
                        case KeyEvent.VK_UP:
                            if (keyReleased && previousIterationAction.isEnabled())
                                previousIterationAction.actionPerformed(null);
                            break;
                        case KeyEvent.VK_DOWN:
                            if (keyReleased && nextIterationAction.isEnabled())
                                nextIterationAction.actionPerformed(null);
                            break;
                        default:
                            return false;
                    }
                    return true;
                });
    }

    /**
     * Sets up all {@link Action} properties used in application. (key bindings, icons...)
     */
    private void setUpActions() {
        loadGraphAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
    }

    /**
     * Creates {@link Graph} and {@link View} objects. View is visual
     * representation of Graph and can be put in Swing application as oridinary {@link Component}.
     */
    private void createGraphView() {
        Graph graph = new MultiGraph("Bellman-Ford");
        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");
        this.graph = graph;
        this.smen = new SpriteManager(graph);

        Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
        viewer.enableAutoLayout(Layouts.newLayoutAlgorithm());
        View view = viewer.addDefaultView(false);
        view.setMouseManager(new SpriteIgnoreMouseManager());
        view.addMouseListener(this);

        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createMatteBorder(0, 10, 20, 10, Color.WHITE));
        panel.add((Component) view);
        add(panel);
        this.view = view;
    }

    /**
     * Creates and initializes top menu bar with
     * single menu item.
     */
    private void createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        JMenuItem loadFile = new JMenuItem(loadGraphAction);
        fileMenu.add(loadFile);
        menuBar.add(fileMenu);

        setJMenuBar(menuBar);
    }

    /**
     * Creates and initializes top bar. This bar contains
     * current iteration/step while in "step examining" mode.
     */
    private void createTopBar() {
        JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        topPanel.setBackground(Color.WHITE);
        topPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 0));

        statusLabelPanel = new RoundedPanel(15);
        statusLabelPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        statusLabelPanel.setOpaque(false);

        statusLabel = new JLabel(generateStatusText(0, 0));
        statusLabel.setFont(statusLabel.getFont().deriveFont(20.0f));
        statusLabelPanel.add(statusLabel);

        setStatusPanelVisible(false);
        topPanel.add(statusLabelPanel);
        add(topPanel, BorderLayout.NORTH);
    }

    /**
     * Creates bottom bar and inserts all the necessary buttons
     * for navigating through steps/iterations, displaying final
     * result or reseting algorithm to state of picking start node.
     */
    private void createBottomBar() {

        JPanel bottomPanel = new JPanel(new GridLayout(1, 3));

        //left

        JPanel leftBottomButtons = new JPanel(new FlowLayout(FlowLayout.LEFT));
        leftBottomButtons.add(new JButton(resetAction));
        bottomPanel.add(leftBottomButtons);

        //middle

        JPanel middleBottomButtons = new JPanel(new FlowLayout());
        middleBottomButtons.add(new JButton(previousIterationAction));
        middleBottomButtons.add(new JButton(previousStepAction));
        middleBottomButtons.add(new JButton(nextStepAction));
        middleBottomButtons.add(new JButton(nextIterationAction));
        bottomPanel.add(middleBottomButtons);

        // right

        JPanel righBottomButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        righBottomButtons.add(new JButton(finalSolutionAction));
        bottomPanel.add(righBottomButtons);

        //final

        add(bottomPanel, BorderLayout.SOUTH);
    }

    /**
     * Loads graph from {@link IBellmanFord} model and displays
     * it on UI {@link View} object.
     */
    private void loadGraphView() {

        BFGraph g = model.getGraph();

        graph.clear();
        smen = new SpriteManager(graph);
        graph.addAttribute("ui.stylesheet", Utilites.loadFileFromResources("style.css"));

        g.getNodes().values().forEach(node -> {
            String id = node.getId();
            Node n = graph.addNode(id);
            n.addAttribute("ui.label", id);

            Sprite nodeOld = smen.addSprite(id + "old");
            nodeOld.attachToNode(id);
            nodeOld.setPosition(StyleConstants.Units.PX, 18, 0, 120);
            nodeOld.setAttribute("ui.class", "old");

            Sprite nodeCurrent = smen.addSprite(id + "current");
            nodeCurrent.attachToNode(id);
            nodeCurrent.setPosition(StyleConstants.Units.PX, 18, 0, 60);
            nodeCurrent.setAttribute("ui.class", "current");
        });

        g.getEdges().values().forEach(edge -> graph.addEdge(
                edge.getEdgeId(),
                edge.getStart().getId(),
                edge.getEnd().getId(),
                true)
                .addAttribute("ui.label", edge.getWeight()));

        numOfStepsInIteration = graph.getNodeCount();
        numOfIterations = numOfStepsInIteration - 1;
    }

    /**
     * Gets called when user selects beginning node in graph drawn on UI.
     *
     * @param node Node ID of user selected node
     */
    private void selectStartNode(Node node) {
        if (startNode == null) {
            String error = model.compute(node.getId());
            if (!error.isEmpty()) {
                displayPopUp(error);
            } else {
                startNode = node;
                node.addAttribute("ui.class", "start");
                presentAlgorithmStep(model.nextStep());
            }
        }
    }

    /**
     * Presents all shortest paths found from selected beginning node to all reachable nodes.
     * Paths are highlighted with red color.
     */
    private void presentResult() {
        deselectAllEdges();
        deselectAllNodes();
        resetAllSprites("old");
        setButtonsState(false, false, false, false, true, true);
        setStatusPanelVisible(false);

        fillNodesWithData(model.lastStep().getDistances());

        graph.getNodeSet().stream().map(Node::getId).forEach(id -> {
            if (id.equals(startNode.getId())) return;
            model.getShortestPath(startNode.getId(), id).forEach(edge -> {
                graph.getEdge(edge.getEdgeId()).setAttribute("ui.class", "selected");
                graph.getNode(edge.getEnd().getId()).setAttribute("ui.class", "path");
            });
        });
    }

    /**
     * Presents given {@link AlgorithmStep} on user interface.
     *
     * @param step {@link AlgorithmStep}
     */
    private void presentAlgorithmStep(AlgorithmStep step) {

        setStatusPanelVisible(true);

        if (step.getIteration() == 1 && step.getStep() == 1) {
            setButtonsState(true, false, true, false, true, true);
        } else if (step.getIteration() == numOfIterations && step.getStep() == numOfStepsInIteration) {
            setButtonsState(false, true, false, true, true, true);
        } else {
            setButtonsState(true, true, true, true, true, true);
        }

        deselectAllEdges();
        deselectAllNodes();
        resetAllSprites("old");

        statusLabel.setText(generateStatusText(step.getIteration(), step.getStep()));

        //color edges and nodes connected to base node
        step.getConnectedEdgeList().forEach(edge -> {
            Node endNode = graph.getNode(edge.getEnd().getId());
            String endNodeId = endNode.getId();
            List<String> style = new LinkedList<>(Collections.singletonList("child"));
            if (startNode.getId().equals(endNodeId)) style.add("start");
            endNode.setAttribute("ui.class", style.toArray());
            graph.getEdge(edge.getEdgeId()).setAttribute("ui.class", "selected");
        });

        fillNodesWithData(step.getDistances());

        //color base node
        String id = step.getNode().getId();
        List<String> style = new LinkedList<>(Collections.singletonList("selected"));
        if (id.equals(startNode.getId())) style.add("start");
        graph.getNode(id).addAttribute("ui.class", style.toArray());

    }

    /**
     * Fills all graph nodes with given distances.
     *
     * @param distances List of distances from beginning to all other nodes
     */
    private void fillNodesWithData(List<BFDistance> distances) {
        distances.forEach(distance -> {
            Node node = graph.getNode(distance.getNode().getId());
            String old = distance.getLastValueString();
            String current = distance.getCurrentValueString();
            smen.getSprite(node.getId() + "current").setAttribute("ui.label", current);
            if (!old.equals(current)) smen.getSprite(node.getId() + "old").setAttribute("ui.label", old);
        });
    }

    /**
     * Resets all sprites with given id to empty string.
     *
     * @param id {@link Sprite} id
     */
    private void resetAllSprites(String id) {
        graph.forEach(node -> {
            if (node.getId().equals(startNode.getId())) return;
            Sprite s = smen.getSprite(node.getId() + id);
            s.setAttribute("ui.label", "");
        });
    }

    /**
     * Removes border and fill color from all nodes on graph.
     */
    private void deborderAllNodes() {
        graph.forEach(node -> node.setAttribute("ui.class", ""));
    }

    /**
     * Removes fill color from all nodes on graph.
     */
    private void deselectAllNodes() {
        graph.getEachNode().forEach(node -> {
            String style = node.getId().equals(startNode.getId()) ? "start" : "";
            node.setAttribute("ui.class", style);
        });
    }

    /**
     * Removes fill color from all edges on graph.
     */
    private void deselectAllEdges() {
        graph.getEachEdge().forEach(edge -> edge.setAttribute("ui.class", ""));
    }


    /**
     * Displays given message as pop-up windows.
     *
     * @param error Error message
     */
    private void displayPopUp(String error) {
        JOptionPane.showMessageDialog(UserInterface.this, error, "Warning", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Shows or hides iteration/step number status panel.
     *
     * @param visible If flag is set panel is shown, otherwise it gets hidden.
     */
    private void setStatusPanelVisible(boolean visible) {
        statusLabelPanel.setBackground(visible ? new Color(241, 240, 239) : Color.WHITE);
        statusLabelPanel.setForeground(visible ? Color.BLACK : Color.WHITE);
        statusLabel.setForeground(visible ? Color.BLACK : Color.WHITE);
    }

    /**
     * Generates status text in String form from given current iteration/step data.
     * This string will be used to fill status label on UI.
     *
     * @param currentIteration Current iteration number
     * @param currentStep      Current step number
     * @return Returns status message as String
     */
    private String generateStatusText(int currentIteration, int currentStep) {
        Integer maxSteps = graph.getNodeCount();
        Integer maxIterations = maxSteps - 1;
        int maxStepsWidth = maxSteps.toString().length();
        int maxIterationsWidth = maxIterations.toString().length();
        return String.format("Iteration: %" + maxIterationsWidth + "d/%d     Step: %" +
                maxStepsWidth + "d/%d", currentIteration, maxIterations, currentStep, maxSteps);

    }

    /**
     * Sets enabled state for all the buttons on bottom panel (6 of them) separately.
     *
     * @param nextStep Next step button enabled flag
     * @param prevStep Previous step button enabled flag
     * @param nextIter Next iteration button enabled flag
     * @param prevIter Previous iteration button enabled flag
     * @param reset    Reset button enabled flag
     * @param result   Result button enabled flag
     */
    private void setButtonsState(boolean nextStep, boolean prevStep, boolean nextIter, boolean prevIter, boolean reset, boolean result) {
        previousStepAction.setEnabled(prevStep);
        previousIterationAction.setEnabled(prevIter);
        nextStepAction.setEnabled(nextStep);
        nextIterationAction.setEnabled(nextIter);
        finalSolutionAction.setEnabled(result);
        resetAction.setEnabled(reset);
    }


    /**
     * Logic for moving to previous algorithm step and presenting it.
     */
    private Action previousStepAction = new AbstractAction("", Icons.previousStep) {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (startNode != null) {
                presentAlgorithmStep(model.previousStep());
            }
        }
    };

    /**
     * Logic for moving to next algorithm step and presenting it.
     */
    private Action nextStepAction = new AbstractAction("", Icons.nextStep) {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (startNode != null) {
                presentAlgorithmStep(model.nextStep());
            }
        }
    };


    /**
     * Logic for moving to previous algorithm iteration and presenting it.
     */
    private Action previousIterationAction = new AbstractAction("", Icons.previousIteration) {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (startNode != null) {
                AlgorithmStep step = model.previousIterationFirstStep();
                presentAlgorithmStep(step);
            }
        }
    };

    /**
     * Logic for moving to next algorithm iteration and presenting it.
     */
    private Action nextIterationAction = new AbstractAction("", Icons.nextIteration) {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (startNode != null) {
                AlgorithmStep step = model.nextIterationFirstStep();
                presentAlgorithmStep(step);
            }
        }
    };

    /**
     * Logic for presenting final solution / going back to steps mode.
     */
    private StatefulAbstractAction finalSolutionAction = new StatefulAbstractAction(
            this::presentResult,
            Icons.finalSolution,
            () -> presentAlgorithmStep(model.firstStep()),
            Icons.rewind
    );

    /**
     * Resets view to state where user is picking start node.
     */
    private Action resetAction = new AbstractAction("", Icons.reset) {

        @Override
        public void actionPerformed(ActionEvent e) {
            deborderAllNodes();
            deselectAllEdges();
            setStatusPanelVisible(false);
            resetAllSprites("old");
            resetAllSprites("current");
            finalSolutionAction.resetState();
            setButtonsState(false, false, false, false, false, false);
            startNode = null;
        }
    };

    /**
     * Loads new graph from user selected file on disk and displays
     * it on screen.
     */
    private Action loadGraphAction = new AbstractAction("Load graph") {

        @SuppressWarnings("OptionalGetWithoutIsPresent")
        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fc = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Graph descriptors as textual files", "graph");
            fc.setFileFilter(filter);

            int ret = fc.showOpenDialog(UserInterface.this);
            if (ret == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                IBellmanFord newModel = new BellmanFordImpl();

                String error = newModel.parse(file.getAbsolutePath());
                if (!error.isEmpty()) {
                    displayPopUp(error);
                    return;
                }

                model = newModel;
                startNode = null;
                finalSolutionAction.resetState();
                setStatusPanelVisible(false);
                setButtonsState(false, false, false, false, false, false);
                loadGraphView();
            }
        }

    };

    @Override
    public void mouseClicked(MouseEvent e) {
        Collection<GraphicElement> result = view.allNodesOrSpritesIn(e.getX(), e.getY(), e.getX(), e.getY());
        if (result == null) return;
        Collection<GraphicElement> filteredResult = result.stream().filter(el -> !(el instanceof GraphicSprite)).collect(Collectors.toList());
        if (filteredResult.size() == 1) {
            GraphicElement gElement = (GraphicElement) (filteredResult.toArray()[0]);
            if (gElement instanceof Node) {
                Node node = (Node) gElement;
                selectStartNode(node);
            }
        }
    }


}
