package hr.fer.tel.infmre.bellmanford.algorithm;

import hr.fer.tel.infmre.bellmanford.models.AlgorithmStep;
import hr.fer.tel.infmre.bellmanford.models.BFEdge;
import hr.fer.tel.infmre.bellmanford.models.BFGraph;

import java.util.List;

/**
 * Bellman Ford algorithm simulator interface
 * https://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm
 */
public interface IBellmanFord {


    /**
     * Parses input file and informs about the result
     *
     * @param inputFile input file path
     * @return message if error exists, empty string if there is no error
     */
    String parse(String inputFile);

    /**
     * Calculates the entire algorithm that computes shortest paths from
     * a single source vertex to all of the other vertices in a weighted digraph.
     * <b>nodeId</b> must be provided as a parameter.
     *
     * @param nodeId source vertex identifier
     * @return message if error exists, empty string if there is no error
     */
    String compute(String nodeId);

    /**
     * Returns graph with all nodes and edges
     *
     * @return graph, null if algorithm has not been computed yet
     */
    BFGraph getGraph();

    /**
     * This method returns the next step in BellmanFord algorithm. If
     * it is called for the first time, it will return first algorithm
     * step in the list.
     *
     * @return next step, null if there is no next step
     */
    AlgorithmStep nextStep();

    /**
     * This method returns the previous step in BellmanFord algorithm. If
     * it is called for the first time, it will return first algorithm
     * step in the list.
     *
     * @return previous step, null if there is no previous step
     */
    AlgorithmStep previousStep();

    /**
     * This method returns the current step in BellmanFord algorithm. If
     * it is called for the first time, it will return first algorithm
     * step in the list.
     *
     * @return current step
     */
    AlgorithmStep currentStep();


    /**
     * Returns the first step of next iteration. If limit is reached,
     * it returns the last algorithm step.
     *
     * @return first step of next iteration
     */
    AlgorithmStep nextIterationFirstStep();


    /**
     * Returns the first step of previous iteration. If limit is reached,
     * it returns first algorithm step.
     *
     * @return first step of previous iteration
     */
    AlgorithmStep previousIterationFirstStep();


    /**
     * @return first algorithm step
     */
    AlgorithmStep firstStep();

    /**
     * @return last algorithm step
     */
    AlgorithmStep lastStep();

    /**
     * Calculates the shortest path between two nodes.
     *
     * @param startNodeId node id of starting node
     * @param endNodeId   node id of destination node
     * @return list of edges between these two nodes
     */
    List<BFEdge> getShortestPath(String startNodeId, String endNodeId);


}
