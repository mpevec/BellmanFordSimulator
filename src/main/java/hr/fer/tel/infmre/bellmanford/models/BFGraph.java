package hr.fer.tel.infmre.bellmanford.models;

import java.util.Map;

/**
 * This class represents graph in Bellman-Ford algorithm
 * and it contains its nodes and edges.
 *
 * @author Matija Pevec
 */
public class BFGraph {

    private Map<String, BFNode> nodes;
    private Map<String, BFEdge> edges;

    public BFGraph(Map<String, BFNode> nodes, Map<String, BFEdge> edges) {
        this.nodes = nodes;
        this.edges = edges;
    }

    public Map<String, BFNode> getNodes() {
        return nodes;
    }

    public Map<String, BFEdge> getEdges() {
        return edges;
    }

    @Override
    public String toString() {
        return "BFGraph{" +
                "nodes=" + nodes +
                ", edges=" + edges +
                '}';
    }

    public Integer numberOfIterations() {
        return nodes.size() - 1;
    }

    public Integer numberOfSteps() {
        return nodes.size();
    }
}
