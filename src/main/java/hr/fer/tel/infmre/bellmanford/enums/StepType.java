package hr.fer.tel.infmre.bellmanford.enums;


/**
 * All step variants that occur in presentation layer
 * of Bellman-Ford algorithm.
 *
 * @author Matija Pevec
 */
public enum StepType {
    PREVIOUS, CURRENT, NEXT, FIRST, LAST, FIRST_NEXT_ITERATION, FIRST_PREVIOUS_ITERATION;
}
