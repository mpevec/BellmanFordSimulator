package hr.fer.tel.infmre.bellmanford.gui.actions;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Class extends AbstractAction in a way that it adds
 * state. It can be in one of two states. Each state can
 * have it's own Action and ImageIcon.
 * State toggles automatically on each fired action.
 */
public class StatefulAbstractAction extends AbstractAction {

    /**
     * State of this stateful action
     */
    private boolean state = true;

    /**
     * First state action
     */
    private Runnable firstAction;

    /**
     * Second state action
     */
    private Runnable secondAction;

    /**
     * First state image icon
     */
    private ImageIcon firstIcon;

    /**
     * Second state image icon
     */
    private ImageIcon secondIcon;

    /**
     * Constructor initializes new stateful action with given actions and icons.
     *
     * @param firstAction  First action
     * @param firstIcon    First action image icon
     * @param secondAction Second action
     * @param secondIcon   Second action image icon
     */
    public StatefulAbstractAction(Runnable firstAction, ImageIcon firstIcon, Runnable secondAction, ImageIcon secondIcon) {
        super("", firstIcon);
        this.firstAction = firstAction;
        this.secondAction = secondAction;
        this.firstIcon = firstIcon;
        this.secondIcon = secondIcon;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (state) firstAction.run();
        else secondAction.run();
        state = !state;
        putValue(SMALL_ICON, state ? firstIcon : secondIcon);
    }

    /**
     * Resets action state to default state.
     */
    public void resetState() {
        state = true;
        putValue(SMALL_ICON, firstIcon);
    }
}
