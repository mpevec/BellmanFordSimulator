package hr.fer.tel.infmre.bellmanford.models;

import java.util.Objects;

/**
 * This class represents a node in Bellman-Ford algorithm
 * and it contains its id.
 *
 * @author Matija Pevec
 */
public class BFNode {

    private String id;
    private String edgeIdToPreviousNode;

    public BFNode(Integer id) {
        this.id = id.toString();
        this.edgeIdToPreviousNode = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BFNode)) {
            return false;
        }
        BFNode node = (BFNode) o;
        return Objects.equals(id, node.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return id;
    }

    public String getId() {
        return id;
    }

    public String getEdgeIdToPreviousNode() {
        return edgeIdToPreviousNode;
    }

    public void setEdgeIdToPreviousNode(String edgeIdToPreviousNode) {
        this.edgeIdToPreviousNode = edgeIdToPreviousNode;
    }

}
