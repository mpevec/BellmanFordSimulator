package hr.fer.tel.infmre.bellmanford.models;


import java.util.Objects;

/**
 * Class that saves the distance value from previously picked
 * starting node.
 *
 * @author Matija Pevec
 */
public class BFDistance {
    private BFNode node;
    private Integer currentValue;
    private Integer lastValue;

    public BFDistance(BFNode node, Integer currentValue, Integer lastValue) {
        this.node = node;
        this.currentValue = currentValue;
        this.lastValue = lastValue;
    }

    public BFDistance(BFDistance bfDistance) {
        node = bfDistance.getNode();
        currentValue = bfDistance.getCurrentValue();
        lastValue = bfDistance.getLastValue();
    }

    public BFNode getNode() {
        return node;
    }

    public void setNode(BFNode node) {
        this.node = node;
    }

    public Integer getCurrentValue() {
        return currentValue;
    }

    public String getCurrentValueString() {
        return currentValue.equals(Integer.MAX_VALUE) ? "\u221E" : currentValue.toString();
    }

    public void setCurrentValue(Integer currentValue) {
        this.currentValue = currentValue;
    }

    public Integer getLastValue() {
        return lastValue;
    }

    public String getLastValueString() {
        return lastValue.equals(Integer.MAX_VALUE) ? "\u221E" : lastValue.toString();
    }

    public void setLastValue(Integer lastValue) {
        this.lastValue = lastValue;
    }

    @Override
    public String toString() {
        return "BFDistance{" +
                "node=" + node +
                ", currentValue=" + getCurrentValueString() +
                ", lastValue=" + getLastValueString() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BFDistance that = (BFDistance) o;
        return Objects.equals(node, that.node);
    }

    @Override
    public int hashCode() {
        return Objects.hash(node);
    }
}
