package hr.fer.tel.infmre.bellmanford.algorithm.impl;

import hr.fer.tel.infmre.bellmanford.algorithm.IBellmanFord;
import hr.fer.tel.infmre.bellmanford.enums.EdgeType;
import hr.fer.tel.infmre.bellmanford.enums.StepType;
import hr.fer.tel.infmre.bellmanford.models.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Bellman-Ford Algorithm Implementation. This implementation is structured
 * in the way that presentation layer can easily grab all the information needed
 * to perform the algorithm step by step.
 *
 * @author Matija Pevec
 */
public class BellmanFordImpl implements IBellmanFord {

    Map<String, BFNode> nodes = new HashMap<>();
    Map<String, BFEdge> edges = new HashMap<>();
    List<AlgorithmStep> algorithmSteps = new LinkedList<>();
    Integer stepIndex;
    boolean firstStepRequest;

    @Override
    public String parse(String inputFile) {
        List<String> inputLines = null;
        try {
            inputLines = Files.readAllLines(Paths.get(inputFile));
        } catch (IOException e) {
            System.out.println("Unable to read the file...");
            return "Unable to read the file...";
        }

        Set<BFNode> nodeSet = new LinkedHashSet<>();
        Set<BFEdge> edgeSet = new LinkedHashSet<>();

        ListIterator<String> iter = inputLines.listIterator();
        while (iter.hasNext()) {
            String line = iter.next();

            if (line.isEmpty()) continue;

            try {
                String[] lineParts = line.split(";")[0].split(",");

                // Parsing the line
                BFNode node1 = new BFNode(Integer.parseInt(lineParts[0].trim()));
                BFNode node2 = new BFNode(Integer.parseInt(lineParts[1].trim()));
                Integer weight = Integer.parseInt(lineParts[2].trim());

                if (node1.equals(node2)) throw new IllegalArgumentException("(Line " + iter.nextIndex()
                        + ") Edge cannot contain two identical nodes.");

                if (Integer.parseInt(node1.getId()) < 0
                        || Integer.parseInt(node2.getId()) < 0) throw new IllegalArgumentException("(Line "
                        + iter.nextIndex() + ") Node IDs must be non-negative integers.");

                EdgeType edgeType;
                if (lineParts[3].trim().equals("u")) {
                    edgeType = EdgeType.DIRECTED;
                } else if (lineParts[3].trim().equals("n")) {
                    edgeType = EdgeType.UNDIRECTED;
                } else {
                    throw new IllegalArgumentException("(Line " + iter.nextIndex()
                            + ") Edge type must be 'u' for directed or 'n' for undirected.");
                }

                // Saving values from single line
                nodeSet.add(node1);
                nodeSet.add(node2);
                if (!edgeSet.add(new BFEdge(node1, node2, weight, edgeType))) {
                    throw new IllegalArgumentException("(Line " + iter.nextIndex()
                            + ") Edge with these nodes already exists.");
                }
            } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException e) {
                System.out.println("Syntax error in file.\n" + e.getMessage());
                return "Syntax error in file.\n" + e.getMessage();
            }
        }

        if (edgeSet.isEmpty()) {
            System.out.println("File must contain at least one edge.");
            return "File must contain at least one edge.";
        }

        nodeSet.forEach(bfNode -> nodes.put(bfNode.getId(), bfNode));
        edgeSet.forEach(bfEdge -> edges.put(bfEdge.getEdgeId(), bfEdge));

        System.out.println("Nodes: " + nodes);
        System.out.println("Edges: " + edges);

        return "";
    }

    @Override
    public String compute(String nodeId) {

        nodes.values().forEach(bfNode -> bfNode.setEdgeIdToPreviousNode(null));
        algorithmSteps.clear();
        firstStepRequest = true;

        // Checking edge types (all must be directed)
        Long numberOfUndirectedEdges = edges.values().stream()
                .filter(bfEdge -> bfEdge.getEdgeType().equals(EdgeType.UNDIRECTED)).count();

        if (numberOfUndirectedEdges > 0) {
            System.out.println("All graph edges must be directed.");
            return "All graph edges must be directed.";
        }

        // ===================
        // Algorithm computing
        // ===================
        List<BFDistance> distances = new LinkedList<>();
        nodes.forEach((s, bfNode) -> distances.add(new BFDistance(bfNode, Integer.MAX_VALUE, Integer.MAX_VALUE)));
        // Setting starting node
        BFDistance firstDistance = distances.stream()
                .filter(bfDistance -> bfDistance.getNode().getId().equals(nodeId))
                .findFirst().get();
        firstDistance.setCurrentValue(0);
        firstDistance.setLastValue(0);

        Integer numberOfIterations = nodes.size() - 1;
        boolean hasChanged = false;

        // Iterations => (#Node - 1) + 1
        // (#Node - 1) for finding the shortest paths plus 1 for detecting
        // whether the graph has negative cycles
        for (int i = 1; i <= numberOfIterations + 1; i++) {
            hasChanged = false;

            ListIterator<BFDistance> iterator = distances.listIterator();
            while (iterator.hasNext()) {
                BFDistance nodeDist = iterator.next();
                AlgorithmStep step = new AlgorithmStep(i, iterator.nextIndex(), nodeDist.getNode(), new LinkedList<>(), new LinkedList<>());
                List<BFEdge> connectedEdges = edges.values().stream()
                        .filter(bfEdge -> bfEdge.getStart().equals(nodeDist.getNode())).collect(Collectors.toList());
                step.getConnectedEdgeList().addAll(connectedEdges);

                for (BFEdge edge : connectedEdges) {
                    BFDistance destinationNodeDist = distances.stream()
                            .filter(bfDistance -> bfDistance.getNode().equals(edge.getEnd()))
                            .findFirst().get();

                    if (!nodeDist.getCurrentValue().equals(Integer.MAX_VALUE) &&
                            (nodeDist.getCurrentValue() + edge.getWeight() < destinationNodeDist.getCurrentValue())) {
                        Integer lastValue = destinationNodeDist.getCurrentValue();
                        destinationNodeDist.setCurrentValue(nodeDist.getCurrentValue() + edge.getWeight());
                        destinationNodeDist.setLastValue(nodeDist.getCurrentValue() + edge.getWeight());
                        step.getDistances().add(new BFDistance(edge.getEnd(),
                                destinationNodeDist.getCurrentValue(),
                                lastValue));
                        nodes.get(edge.getEnd().getId()).setEdgeIdToPreviousNode(edge.getEdgeId());
                        hasChanged = true;
                    } else {
                        step.getDistances().add(new BFDistance(edge.getEnd(), destinationNodeDist.getCurrentValue(),
                                destinationNodeDist.getCurrentValue()));
                    }

                }


                List<BFDistance> distancesToAdd = distances.stream()
                        .filter(bfDistance -> !step.getDistances().contains(bfDistance))
                        .collect(Collectors.toList());
                distancesToAdd.forEach(bfDistance -> step.getDistances().add(new BFDistance(bfDistance)));

                // don't add step if it's the last iteration
                if (!(i == (numberOfIterations + 1))) algorithmSteps.add(step);
            }
        }

        System.out.println("Algorithm steps: ");
        algorithmSteps.forEach(algorithmStep -> System.out.println("  " + algorithmStep));
        System.out.println("Algorithm iterations: " + numberOfIterations);
        System.out.println("Number of Algorithm steps: " + algorithmSteps.size());

        if (hasChanged) {
            System.out.println("Graph must have non-negative cycles.");
            return "Graph must have non-negative cycles.";
        } else {
            return "";
        }
    }

    @Override
    public BFGraph getGraph() {
        return edges.isEmpty() ? null : new BFGraph(nodes, edges);
    }

    @Override
    public AlgorithmStep nextStep() {
        return stepHandler(StepType.NEXT);
    }

    @Override
    public AlgorithmStep previousStep() {
        return stepHandler(StepType.PREVIOUS);
    }

    @Override
    public AlgorithmStep currentStep() {
        return stepHandler(StepType.CURRENT);
    }

    @Override
    public AlgorithmStep nextIterationFirstStep() {
        return stepHandler(StepType.FIRST_NEXT_ITERATION);
    }

    @Override
    public AlgorithmStep previousIterationFirstStep() {
        return stepHandler(StepType.FIRST_PREVIOUS_ITERATION);
    }

    @Override
    public AlgorithmStep firstStep() {
        return stepHandler(StepType.FIRST);
    }

    @Override
    public AlgorithmStep lastStep() {
        firstStepRequest = false;
        return stepHandler(StepType.LAST);
    }

    @Override
    public List<BFEdge> getShortestPath(String startNodeId, String endNodeId) {
        List<BFEdge> edgeList = new LinkedList<>();
        BFNode node = nodes.get(endNodeId);
        BFEdge edge;
        while (true) {
            if ((edge = edges.get(node.getEdgeIdToPreviousNode())) == null) break;
            edgeList.add(edge);
            node = nodes.get(edge.getStart().getId());
            if (node.getId().equals(startNodeId)) break;
        }

        return edgeList;
    }


    /**
     * Custom logic to get requested algorithm step depending
     * on algorithm status.
     *
     * @param stepType previous, next, current, first or last step
     * @return requested algorithm step
     */
    private AlgorithmStep stepHandler(StepType stepType) {
        if (firstStepRequest) {
            firstStepRequest = false;
            stepIndex = 0;
            return algorithmSteps.get(stepIndex);

        } else {
            try {
                switch (stepType) {
                    case NEXT:
                        return algorithmSteps.get(++stepIndex);
                    case PREVIOUS:
                        return algorithmSteps.get(--stepIndex);
                    case CURRENT:
                        return algorithmSteps.get(stepIndex);
                    case FIRST:
                        return algorithmSteps.get(stepIndex = 0);
                    case LAST:
                        return algorithmSteps.get(stepIndex = (algorithmSteps.size() - 1));
                    case FIRST_PREVIOUS_ITERATION:
                        Integer iteration = currentStep().getIteration();
                        AlgorithmStep currentStep = currentStep();
                        while ((previousStep()) != currentStep) {
                            if (currentStep().getIteration() < iteration - 1) return nextStep();
                            currentStep = currentStep();
                        }
                        return firstStep();
                    case FIRST_NEXT_ITERATION:
                        Integer iter = currentStep().getIteration();
                        AlgorithmStep currStep = currentStep();
                        while ((nextStep()) != currStep) {
                            if (currentStep().getIteration() > iter) return currentStep();
                            currStep = currentStep();
                        }
                        return lastStep();
                    default:
                        return null;
                }
            } catch (IndexOutOfBoundsException e) {
                stepIndex = stepIndex > algorithmSteps.size() - 1 ? algorithmSteps.size() - 1 : stepIndex;
                stepIndex = stepIndex < 0 ? 0 : stepIndex;
                return currentStep();
            }

        }
    }
}


















