package hr.fer.tel.infmre.bellmanford.models;

import java.util.List;

/**
 * This class contains all the information about one
 * algorithm step.
 *
 * @author Matija Pevec
 */
public class AlgorithmStep {

    private Integer iteration;
    private Integer step;
    private BFNode node;
    private List<BFEdge> connectedEdgeList;
    private List<BFDistance> distances;

    public AlgorithmStep() {
    }

    public AlgorithmStep(Integer iteration, Integer step,
                         BFNode node, List<BFEdge> connectedEdgeList,
                         List<BFDistance> distances) {
        this.iteration = iteration;
        this.step = step;
        this.node = node;
        this.connectedEdgeList = connectedEdgeList;
        this.distances = distances;
    }

    public Integer getIteration() {
        return iteration;
    }

    public Integer getStep() {
        return step;
    }

    public BFNode getNode() {
        return node;
    }

    public List<BFEdge> getConnectedEdgeList() {
        return connectedEdgeList;
    }

    public List<BFDistance> getDistances() {
        return distances;
    }

    @Override
    public String toString() {
        return "AlgorithmStep{" +
                "iteration=" + iteration +
                ", step=" + step +
                ", node=" + node +
                ", distances=" + distances +
                ", connectedEdgeList=" + connectedEdgeList +
                '}';
    }
}
